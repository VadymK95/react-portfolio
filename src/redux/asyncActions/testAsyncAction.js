import { addUsersAction } from '../actions/testActions';

export const getAsyncUsers = () => (dispatch) => {
  fetch('https://jsonplaceholder.typicode.com/users?_limit=10')
    .then((response) => response.json())
    .then((json) => dispatch(addUsersAction(json)))
    .catch((err) => new Error(err));
};
