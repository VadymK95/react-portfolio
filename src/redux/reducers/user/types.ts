import { EUser } from '../../constants/user';

export interface IUser {
  email: string;
  id: number;
  token: string;
}

interface TypeSetUser {
  type: EUser.SET_USER;
  payload: IUser;
};

interface TypeRemoveUser {
  type: EUser.REMOVE_USER;
};

export type Action = TypeSetUser | TypeRemoveUser;