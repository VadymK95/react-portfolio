import { EUser } from '../../constants/user';

import { Action, IUser } from './types';

const initialState = {
  email: null,
  token: null,
  id: null,
};

export const userReducer = (state = initialState, action: Action) => {
  switch (action.type) {
  case EUser.SET_USER:
    return {
      ...state,
      email: action.payload.email,
      token: action.payload.token,
      id: action.payload.id,
    };
  case EUser.REMOVE_USER:
    return {
      ...state,
      email: null,
      token: null,
      id: null,
    };
  default:
    return state;
  }
};

export const setUserAction = (payload: IUser) => ({type: EUser.SET_USER, payload});
export const removeUserAction = () => ({ type: EUser.SET_USER });
