import { ADD_TEXT, ADD_USERS, RESET_TEXT } from '../constants/testConstants';

export const addTextAction = (payload) => ({ type: ADD_TEXT, payload });
export const resetAction = () => ({ type: RESET_TEXT });
export const addUsersAction = (payload) => ({ type: ADD_USERS, payload });
