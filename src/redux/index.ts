import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import { userReducer } from './reducers/user/userReducer';

const rootReducer = combineReducers({
  userReducer: userReducer,
});

export const store = createStore(rootReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof rootReducer>
export type AppDispatch = typeof store.dispatch;
