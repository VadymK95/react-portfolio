import { FC, useEffect } from 'react';

import { Header, Sidebar } from './components/index';
import { RouterApp } from './router/RouterApp';

const App: FC = () => {
  useEffect(() => {
    if (!localStorage.getItem('users')) {
      localStorage.setItem('users', JSON.stringify([]));
    }
  }, []);

  return (
    <div className="app">
      <Header />
      <Sidebar />
      <RouterApp />
    </div>
  );
};

export default App;
