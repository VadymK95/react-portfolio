import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { getUsersFromLS } from '../../api/getUsersFromLS';
import { CustomLink, Input, Button } from '../../components';
import { IUser } from '../../types';

export const SignUp: FC = () => {
  const [loginvalue, setLoginValue] = useState('');
  const [passwordValue, setPasswordValue] = useState('');
  const [passwordRepeatValue, setPasswordRepeatValue] = useState('');
  const [isValidPassword, setIsValidPassword] = useState(false);
  const [isValidLogin, setIsValidLogin] = useState(false);
  const [isExist, setIsExist] = useState(false);

  const navigate = useNavigate();

  const handleLogin = (e: React.ChangeEvent<EventTarget>) =>
    setLoginValue((e.target as HTMLInputElement).value);

  const handlePassword = (e: React.ChangeEvent<EventTarget>) =>
    setPasswordValue((e.target as HTMLInputElement).value);

  const handleRepeatPassword = (e: React.ChangeEvent<EventTarget>) =>
    setPasswordRepeatValue((e.target as HTMLInputElement).value);

  const handleCreate = (e: React.MouseEvent) => {
    e.preventDefault();
    loginvalue && loginvalue.length >= 5
      ? setIsValidLogin(false)
      : setIsValidLogin(true);

    passwordValue === passwordRepeatValue && passwordValue.length > 0
      ? setIsValidPassword(false)
      : setIsValidPassword(true);

    if (
      passwordValue === passwordRepeatValue &&
      loginvalue.length >= 5 &&
      passwordValue.length >= 5
    ) {
      const user = {
        email: loginvalue,
        password: passwordValue,
        id: Date.now(),
      };

      if (getUsersFromLS().find((el: IUser) => el.email === user.email)) {
        return setIsExist(true);
      }
      localStorage.setItem(
        'users',
        JSON.stringify([...getUsersFromLS(), user]),
      );
      setLoginValue('');
      setPasswordValue('');
      setPasswordRepeatValue('');
      setIsExist(false);
      navigate('/login');
    }
  };

  return (
    <form>
      <fieldset className="flex items-center flex-col">
        <div className="w-1/3">
          <label className="flex flex-col">
            <p className="text-xs">Login</p>
            <Input
              value={loginvalue}
              onInputChange={handleLogin}
              placeholder="enter a new login"
              type="email"
            />
          </label>
          <p
            className={`${
              isExist || isValidLogin ? 'opacity-100' : 'opacity-0'
            } error-text`}
          >
            {isExist && 'This login already exist!'}
            {isValidLogin && 'There is a problem with login'}
          </p>
        </div>
        <div className="w-1/3 pt-4">
          <label className="flex flex-col">
            <p className="text-xs">Password</p>
            <Input
              type="password"
              value={passwordValue}
              onInputChange={handlePassword}
              placeholder="enter a new password"
            />
          </label>
        </div>
        <div className="w-1/3 pt-4 pb-8">
          <label className="flex flex-col">
            <p className="text-xs">Repeat password</p>
            <Input
              type="password"
              value={passwordRepeatValue}
              onInputChange={handleRepeatPassword}
              placeholder="repeat your password"
            />
          </label>
          <p
            className={`${
              isValidPassword ? 'opacity-100' : 'opacity-0'
            } error-text`}
          >
            Check your password!
          </p>
        </div>
        <Button
          name="Create an Account"
          handleOnClick={(e) => handleCreate(e)}
        />
        <p>
          <span>Already have an account? </span>
          <CustomLink
            styles="primary-color underline"
            path="/login"
            name="Login"
          />
        </p>
      </fieldset>
    </form>
  );
};
