import { FC } from 'react';
import './styles.css';

interface IButton {
  name: string;
  handleOnClick: (...args: any) => void;
  styles?: string;
};

export const Button: FC<IButton> = ({ name, handleOnClick, styles }) => {
  return (
    <button className={`${styles} btn`} onClick={handleOnClick}>
      {name}
    </button>
  );
};
