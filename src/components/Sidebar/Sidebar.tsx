import { FC } from 'react';
import { useLocation } from 'react-router-dom';

import { CustomLink } from '../index';
import { links } from '../../router/links';

import styles from './Sidebar.module.css';

export const Sidebar: FC = () => {
  const location = useLocation();

  const isLoginPath: boolean =
    location.pathname !== '/login' && location.pathname !== '/register';

  return (
    <nav className="sidebar">
      <ul>
        {isLoginPath &&
          links.map((link) => (
            <li key={link.name} className={styles.item}>
              <CustomLink
                styles="text-white"
                path={link.path}
                name={link.name}
              />
            </li>
          ))}
      </ul>
    </nav>
  );
};
