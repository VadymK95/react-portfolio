import { FC } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';

import { getAuth } from '../../api/getAuth';
import { CustomLink } from '../CustomLink/CustomLink';

import styles from './Header.module.css';


export const Header: FC = () => {
  const navigate = useNavigate();
  const isAuth: boolean = JSON.parse(localStorage.getItem('auth')!);

  const logOut = () => {
    getAuth(false);
    navigate('/login');
  };

  return (
    <header className="header">
      <div className={styles.headerWrapp}>
        <NavLink to="/">
          <img
            className={styles.image}
            src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
            alt="logo"
          />
        </NavLink>
        {isAuth && (
          <CustomLink
            handleClick={logOut}
            styles="text-white"
            path="/login"
            name="Log Out"
          />
        )}
      </div>
    </header>
  );
};
