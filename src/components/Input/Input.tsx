import React, { FC } from 'react';
import './Input.css';

interface IInput {
  type?: string;
  onInputChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  inputStyles?: string;
  placeholder?: string;
}

export const Input: FC<IInput> = ({
  type = 'text',
  placeholder = '',
  value = '',
  onInputChange,
  inputStyles = '',
}) => {
  return (
    <input
      className={`${inputStyles} input`}
      type={type}
      placeholder={placeholder}
      value={value}
      onChange={onInputChange}
    />
  );
};
