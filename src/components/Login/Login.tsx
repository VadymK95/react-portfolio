import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { getUsersFromLS } from '../../api/getUsersFromLS';
import { Button, CustomLink, Input } from '../../components';
import { IUser } from '../../types';
import { getAuth } from '../../api/getAuth';

export const Login: FC = () => {
  const navigate = useNavigate();

  const [loginvalue, setLoginValue] = useState('');
  const [passwordvalue, setPasswordValue] = useState('');
  const [isValid, setIsValid] = useState(false);

  const handleLogin = (e: React.ChangeEvent<EventTarget>) =>
    setLoginValue((e.target as HTMLInputElement).value);

  const handlePassword = (e: React.ChangeEvent<EventTarget>) =>
    setPasswordValue((e.target as HTMLInputElement).value);

  const handleClickLogin = (e: React.MouseEvent) => {
    e.preventDefault();
    const auth = !!getUsersFromLS().find(
      (u: IUser) => u.email === loginvalue && u.password === passwordvalue,
    );

    if (auth) {
      getAuth(true);
      setIsValid(false);
      return navigate('/');
    }
    setIsValid(true);
  };

  return (
    <form>
      <fieldset className="flex items-center flex-col">
        <div className="w-1/3 pt-4 w-1/3">
          <label className="flex flex-col">
            <p className="text-xs">Login</p>
            <Input
              value={loginvalue}
              onInputChange={handleLogin}
              placeholder="enter your login"
              type="email"
            />
          </label>
        </div>
        <div className="w-1/3 pt-4 w-1/3">
          <label className="flex flex-col">
            <p className="text-xs">Password</p>
            <Input
              type="password"
              value={passwordvalue}
              onInputChange={handlePassword}
              placeholder="enter your password"
            />
          </label>
        </div>
        <p
          className={`${
            isValid ? 'opacity-100' : 'opacity-0'
          } error-text pb-8`}
        >
          Login or Password is not right!
        </p>
        <Button name="Sign up" handleOnClick={(e) => handleClickLogin(e)} />
        <p>
          <span>or </span>
          <CustomLink
            styles="primary-color underline"
            path="/register"
            name="Register"
          />
        </p>
      </fieldset>
    </form>
  );
};
