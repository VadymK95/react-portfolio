import React, { FC } from 'react';
import { NavLink } from 'react-router-dom';

type CustomLinkType = {
  path: string;
  name: string;
  children?: React.FC;
  styles?: string;
  handleClick?: () => void;
};

export const CustomLink: FC<CustomLinkType> = ({
  path,
  name,
  children,
  styles = '',
  handleClick,
}) => {
  return (
    <NavLink onClick={handleClick} className={styles} to={path}>
      {name ? name : children}
    </NavLink>
  );
};
