import { IUser } from '../types';

export const getUsersFromLS = (): IUser[] => JSON.parse(localStorage.getItem('users') || '[]');
