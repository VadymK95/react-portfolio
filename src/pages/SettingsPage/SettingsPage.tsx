import { FC } from 'react';

export const SettingsPage: FC = () => {
  return (
    <main className="settings">
      <header className="settings__header">
        <h1 className="settings__title title">Settings</h1>
      </header>
      <section className="settings__content"></section>
    </main>
  );
};
