import React, { FC } from 'react';
import './styles.css';

export const ErrorPage: FC = () => {
  return (
    <main className="error-page">
      <header className="error-page__header">
        <h1 className="error-page__title title">404 not found</h1>
      </header>
      <section className="error-page__content"></section>
    </main>
  );
};
