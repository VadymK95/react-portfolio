import { FC } from 'react';

export const HomePage: FC = () => {
  return (
    <main className="home">
      <header className="home__header">
        <h1 className="home__title title">Home Page</h1>
      </header>
    </main>
  );
};
