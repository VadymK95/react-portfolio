import { FC } from 'react';

import { SignUp } from '../../components';

export const RegisterPage: FC = () => {
  return (
    <div className="register">
      <h1 className="register__title title text-center pt-16 pb-8">Register</h1>
      <SignUp />
    </div>
  );
};
