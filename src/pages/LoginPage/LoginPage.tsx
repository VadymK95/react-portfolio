import { FC } from 'react';

import { Login } from '../../components';

export const LoginPage: FC = () => {
  return (
    <div className="login">
      <h1 className="login__title title text-center pt-16 pb-8">Login</h1>
      <Login />
    </div>
  );
};
