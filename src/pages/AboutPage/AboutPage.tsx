import React, { FC, useState } from 'react';

import './styles.css';
import { Input } from '../../components/Input/Input';
import { Button } from '../../components';

interface ITodo {
  id: number;
  title: string;
  completed: boolean;
}

export const AboutPage: FC = () => {
  const [inputValue, setInputValue] = useState('');
  const [todos, setTodos] = useState([] as ITodo[]);

  const inputHandler = (e: React.ChangeEvent<EventTarget>) =>
    setInputValue((e.target as HTMLInputElement).value);

  const handleClick = () => {
    if (!inputValue) return;
    const todo = {
      id: Date.now(),
      title: inputValue,
      completed: false,
    };

    setTodos([...todos, todo]);
    setInputValue('');
  };

  const removeClick = (id: number): void =>
    setTodos(todos.filter((todo) => todo.id !== id));

  const handleCheckbox = (todo: ITodo) => {
    todo.completed = !todo.completed;
  };

  return (
    <main className="about">
      <header className="about__header">
        <h1 className="about__title title">About</h1>
      </header>
      <section className="about__content">
        <Input value={inputValue} onInputChange={inputHandler} />
        <Button styles="ml-4" name="Add todo" handleOnClick={handleClick} />
        <ul className="pt-4">
          {todos.map((todo: ITodo) => (
            <li
              className="border p-4 w-1/2 flex justify-between items-center mt-4"
              key={todo.id}
            >
              <Input
                value={''}
                type="checkbox"
                inputStyles="cursor-pointer"
                onInputChange={() => handleCheckbox(todo)}
              />
              <p>{todo.title}</p>
              <Button
                styles="ml-4"
                name="Remove todo"
                handleOnClick={() => removeClick(todo.id)}
              />
            </li>
          ))}
        </ul>
      </section>
    </main>
  );
};
