export { HomePage } from './HomePage/HomePage';
export { AboutPage } from './AboutPage/AboutPage';
export { SettingsPage } from './SettingsPage/SettingsPage';
export { RegisterPage } from './RegisterPage/RegisterPage';
export { LoginPage } from './LoginPage/LoginPage';
export { ErrorPage } from './ErrorPage/ErrorPage';
