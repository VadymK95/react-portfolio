interface ILink {
  name: string;
  path: string;
}

export const links: ILink[] = [
  { name: 'Home', path: '/' },
  { name: 'About', path: '/about' },
  { name: 'Settings', path: '/settings' },
];
