import { FC } from 'react';
import { useRoutes } from 'react-router-dom';

import { routes as listOfRoutes } from './routes';

export const RouterApp: FC = () => {
  const routes = useRoutes(listOfRoutes);
  return <div className="content">{routes}</div>;
};
